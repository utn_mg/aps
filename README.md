# Análisis y Procesamiento de Señales

Temario:
1. [Señales y Sistemas](1.%20Senales%20y%20Sistemas.md)
    - Señales Continuas y Discretas
    - Transformaciones de la variable independiente
    - Señales exponenciales y senoidales
    - Las funciones impulso unitario y escalón unitario
    - Sistemas continuos y discretos
    - Propiedades básicas de los sistemas
2. Sistemas Lineales Invariantes en el Tiempo
    - Introducción
    - Sistemas LTI discretos: La suma de convolución
    - Sistemas LTI continuos: La integral de Convolución
    - Propiedades de los sistemas lineales e invariantes en el tiempo
    - Sistemas LTI causales descritos por ecuaciones diferenciales
    - Funciones Singulares
3. Representación de Señales Periódicas en Series de Fourier
    - Introducción
    - La respuesta de sistemas LTI a exponenciales complejas
    - Representación en series de Fourier de señales periódicas continuas
    - Convergencia de las series de Fourier
    - Propiedades de la serie continua de Fourier
    - Representación en series de Fourier de señales periódicas discretas
    - Propiedades de la serie discreta de Fourier
    - Serie de Fourier y sistemas LTI
    - Filtrado
    - Ejemplos de filtros continuos descritos mediante ecuaciones diferenciales
    - Ejemplos de filtros discretos descritos mediante ecuaciones de diferencias
4. La Transformada Continua de Fourier
    - Introducción
    - Representación de señales aperiódicas: La transformada continua de Fourier
    - La transformada de Fourier para señales periódicas
    - Propiedades de la transformada continua de Fourier
    - La propiedad de convolución
    - La propiedad de multiplicacion
    - Tablas de las propiedades de Fourier y de los pares básicos de transformadas de Fourier
    - Sistemas caracterizados por ecuaciones diferenciales lineales con coeficientes constantes
5. La Transformada de Fourier de tiempo discreto
    - Introducción
    - Representación de señales aperiódicas: La transformada de Fourier de tiempo discreto
    - La transformada de Fourier para señales periódicas
    - Propiedades de la transformada de Fourier de tiempo discreto
    - La propiedad de convolución
    - La propiedad de multiplicación
    - Tablas de las propiedades de la transformada de Fourier y pares básicos de la transformada de Fourier
    - Dualidad
    - Sistemas caracterizados por ecuaciones en diferencias lineales con coeficientes constantes
6. Caracterización en tiempo y frecuencia de señales y sistemas
    - Introducción
    - Representación de la magnitud-fase de la transformada de Fourier
    - Representación de la magnitud-fase de la respuesta en frecuencia de sistemas LTI
    - Propiedades en el dominio del tiempo de filtros ideales selectivos en frecuencia
    - Aspectos en el dominio del tiempo y en el dominio de la frecuencia de los filtros no ideales
    - Sistemas continuos de primer y segundo órdenes
    - Sistemas discretos de primer y segundo órdenes
    - Ejemplo de análisis de sistemas en el dominio del tiempo y de la frecuencia
7. Muestreo
    - Introducción
    - Representación de una señal continua mediante sus muestras: El teorema del muestreo
    - Reconstrucción de una señal a partir de sus muestras usando la interpolación
    - El efecto del muestreo: Traslape
    - Procesamiento discreto de señales continuas
    - Muestreo de señales discretas
8. Sistemas de Comunicación
    - Introducción
    - Modulación de amplitud con exponencial compleja y senoidal
    - Demodulación para AM senoidal
    - Multiplexaje por división de frecuencia
    - Modulación de amplitud senoidal de banda lateral única
    - Modulación de amplitud con una portadora de tren de pulsos
    - Modulación de amplitud de pulsos
    - Modulación de frecuencia senoidal
    - Modulación discreta
9. La Transformada de Laplace
    - Introducción
    - La transformada de Laplace
    - La región de convergencia para las transformadas de Laplace
    - La transformada inversa de Laplace
    - Evaluación geométrica de la transformada de Fourier a partir del diagrama de polos y ceros
    - Propiedades de la transformada de Laplace
    - Algunos pares de transformadas de Laplace
    - Análisis y caracterización de los sistemas LTI usando la transformada de Laplace
    - Álgebra de la función del sistema y representación en diagrama de bloques
    - La transformada unilateral de Laplace
10. La Transformada Z
    - Introducción
    - La transformada z
    - La región de convergencia de la transformada z
    - La transformada z inversa
    - Evaluación geométrica de la transformada de Fourier a partir del diagrama de polos y ceros
    - Propiedades de la transformada z
    - Algunos pares comunes de transformada z
    - Análisis y caracterización de los sistemas LTI usando las transformadas z
    - Álgebra de función del sistema y representaciones en diagramas de bloques
    - La transformada z unilateral
11. Sistemas Lineales Retroalimentados
    - Introducción
    - Sistemas lineales retroalimentados
    - Algunas aplicaciones y consecuencias de la retroalimentación
    - Análisis del lugar geométrico de las raíces de los sistemas lineales retroalimentados
    - El criterio de estabilidad de Nyquist
    - Márgenes de ganancia y fase
12. Expansión en Fracciones Parciales

Referencias:

- Alan V. Oppenheim, Alan S. Willsky (1997), Señales y Sistemas, Segunda Edición, Prentice Hall
- Edward W. Kamen, Bonnie S. Heck, Fundamentos de Señales y Sistemas usando la Web y Matlab, Tercera Edición, Prentice Hall